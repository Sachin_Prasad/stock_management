package Cal;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import static javax.swing.SwingConstants.RIGHT;
import javax.swing.UIManager;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;

public class Calculator extends JFrame implements ActionListener,KeyListener,WindowListener {

    public Calculator() {
        MainComponents();
        try {
            Image icon = Toolkit.getDefaultToolkit().getImage("src/images/icon.jpg");
            setIconImage(icon);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(new NimbusLookAndFeel());
        } catch (Exception e){
        }
        Calculator a = new Calculator();
    }
    int ButtonWidth = 50;
    int ButtonWidth1 = 60;
    int ButtonHeight = 50;

    int x1 = 17;
    int x2 = x1 + ButtonWidth;
    int x3 = x2 + ButtonWidth;
    int x4 = x3 + ButtonWidth;
    int x5 = x4 + ButtonWidth;
    int x6 = x5 + ButtonWidth;
    int x7 = x6 + ButtonWidth;
    int x8 = x7 + ButtonWidth;
    int x9 = x8 + ButtonWidth;
    int x10 = x9 + ButtonWidth;
    int x11 = x10 + ButtonWidth;
    int x12 = x11 + ButtonWidth;

    int y1 = 90;
    int y2 = y1 + ButtonHeight;
    int y3 = y2 + ButtonHeight;
    int y4 = y3 + ButtonHeight;
    int y5 = y4 + ButtonHeight;

    int v1;
    double memorySave;
    double value1, value2, sum;
    String operator;
    double ix, iy;
    double a;
    boolean op = false;

    JTextField dis;
    JButton b1;
    JButton b2;
    JButton b3;
    JButton b4;
    JButton b5;
    JButton b6;
    JButton b7;
    JButton b8;
    JButton b9;
    JButton b10;
    JButton b11;
    JButton b12;
    JButton b13;
    JButton b14;
    JButton b15;
    JButton b16;
    JButton b17;
    JButton b18;
    JButton b19;
    JButton b20;
    JButton b21;
    JButton b22;
    JButton b23;
    JButton s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, s14, s15, s16, s17, s18, s19, s20, s21, s22, s23, s24, s25, s26, s27, s28, s29, s30;
    double mplus, ss, E;
    double mmine = 0;

    public void MainComponents() {
        setTitle("Calculator");
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((d.width - 650) / 2, (d.height - 400) / 2, 650, 400);
        setLayout(null);
        setResizable(false);
        addWindowListener(this);

        dis = new JTextField("0");
        dis.setBounds(13, 10, 600, 50);
        dis.setHorizontalAlignment(RIGHT);
        dis.setFont(new Font("Times new Roman", 1, 24));
        dis.addKeyListener(this);

        b1 = new JButton("C");
        b1.setBounds(x8, y1, ButtonWidth, ButtonHeight);
        b1.setBackground(Color.DARK_GRAY);
        b1.setForeground(Color.WHITE);
        b1.addActionListener(this);

        b2 = new JButton("7");
        b2.setBounds(x8, y2, ButtonWidth, ButtonHeight);
        b2.addActionListener(this);

        b3 = new JButton("4");
        b3.setBounds(x8, y3, ButtonWidth, ButtonHeight);
        b3.addActionListener(this);

        b4 = new JButton("1");
        b4.setBounds(x8, y4, ButtonWidth, ButtonHeight);
        b4.addActionListener(this);

        b5 = new JButton("00");
        b5.setBounds(x8, y5, ButtonWidth, ButtonHeight);
        b5.addActionListener(this);

        b6 = new JButton("Del");
        b6.setBounds(x9, y1, ButtonWidth, ButtonHeight);
        b6.setBackground(Color.DARK_GRAY);
        b6.setForeground(Color.WHITE);
        b6.addActionListener(this);

        b7 = new JButton("MS");
        b7.setBounds(x10, y1, ButtonWidth, ButtonHeight);
        b7.setBackground(Color.DARK_GRAY);
        b7.setForeground(Color.WHITE);
        b7.addActionListener(this);

        b8 = new JButton("MC");
        b8.setBounds(x11, y1, ButtonWidth, ButtonHeight);
        b8.setBackground(Color.DARK_GRAY);
        b8.setForeground(Color.WHITE);
        b8.addActionListener(this);

        b9 = new JButton("MR");
        b9.setBounds(x12, y1, ButtonWidth, ButtonHeight);
        b9.setBackground(Color.DARK_GRAY);
        b9.setForeground(Color.WHITE);
        b9.addActionListener(this);

        b10 = new JButton("+/-");
        b10.setBounds(x12, y2, ButtonWidth, ButtonHeight);
        b10.setBackground(Color.BLACK);
        b10.setForeground(Color.WHITE);
        b10.addActionListener(this);

        b11 = new JButton("=");
        b11.setBounds(x12, y3, 50, 150);
        b11.setBackground(Color.DARK_GRAY);
        b11.setForeground(Color.WHITE);
        b11.addActionListener(this);

        b12 = new JButton("0");
        b12.setBounds(x9, y5, ButtonWidth, ButtonHeight);
        b12.addActionListener(this);

        b13 = new JButton(".");
        b13.setBounds(x10, y5, ButtonWidth, ButtonHeight);
        b13.addActionListener(this);

        b14 = new JButton("+");
        b14.setBounds(x11, y5, ButtonWidth, ButtonHeight);
        b14.setBackground(Color.BLACK);
        b14.setForeground(Color.WHITE);
        b14.addActionListener(this);

        b15 = new JButton("x");
        b15.setBounds(x11, y2, ButtonWidth, ButtonHeight);
        b15.setBackground(Color.BLACK);
        b15.setForeground(Color.WHITE);
        b15.addActionListener(this);

        b16 = new JButton("/");
        b16.setBounds(x11, y3, ButtonWidth, ButtonHeight);
        b16.setBackground(Color.BLACK);
        b16.setForeground(Color.WHITE);
        b16.addActionListener(this);

        b17 = new JButton("-");
        b17.setBounds(x11, y4, ButtonWidth, ButtonHeight);
        b17.setBackground(Color.BLACK);
        b17.setForeground(Color.WHITE);
        b17.addActionListener(this);

        b18 = new JButton("9");
        b18.setBounds(x10, y2, ButtonWidth, ButtonHeight);
        b18.addActionListener(this);

        b19 = new JButton("6");
        b19.setBounds(x10, y3, ButtonWidth, ButtonHeight);
        b19.addActionListener(this);

        b20 = new JButton("3");
        b20.setBounds(x10, y4, ButtonWidth, ButtonHeight);
        b20.addActionListener(this);

        b21 = new JButton("8");
        b21.setBounds(x9, y2, ButtonWidth, ButtonHeight);
        b21.addActionListener(this);

        b22 = new JButton("5");
        b22.setBounds(x9, y3, ButtonWidth, ButtonHeight);
        b22.addActionListener(this);

        b23 = new JButton("2");
        b23.setBounds(x9, y4, ButtonWidth, ButtonHeight);
        b23.addActionListener(this);

        s1 = new JButton("X");
        s1.setBounds(x1, y1, ButtonWidth1, ButtonHeight);
        s1.setBackground(Color.DARK_GRAY);
        s1.setForeground(Color.BLACK);
        s1.addActionListener(this);

        s2 = new JButton("Y");
        s2.setBounds(x2, y1, ButtonWidth1, ButtonHeight);
        s2.setBackground(Color.DARK_GRAY);
        s2.setForeground(Color.BLACK);
        s2.addActionListener(this);

        s3 = new JButton("M+");
        s3.setBounds(x3, y1, ButtonWidth1, ButtonHeight);
        s3.setBackground(Color.BLACK);
        s3.setForeground(Color.WHITE);
        s3.addActionListener(this);

        s4 = new JButton("M-");
        s4.setBounds(x4, y1, ButtonWidth1, ButtonHeight);
        s4.setBackground(Color.BLACK);
        s4.setForeground(Color.WHITE);
        s4.addActionListener(this);

        s5 = new JButton("Shift");
        s5.setBounds(x5, y1, ButtonWidth1, ButtonHeight);
        s5.setBackground(Color.BLACK);
        s5.setForeground(Color.WHITE);
        s5.addActionListener(this);

        s6 = new JButton("%");
        s6.setBounds(x6, y1, ButtonWidth1, ButtonHeight);
        s6.setBackground(Color.BLACK);
        s6.setForeground(Color.WHITE);
        s6.addActionListener(this);

        s7 = new JButton("E");
        s7.setBounds(x1, y2, ButtonWidth1, ButtonHeight);
        s7.setBackground(Color.BLACK);
        s7.setForeground(Color.WHITE);
        s7.addActionListener(this);

        s8 = new JButton("x^2");
        s8.setText("<html><b>x<sup>2</sup></b><br></html>");
        s8.setBounds(x2, y2, ButtonWidth1, ButtonHeight);
        s8.setBackground(Color.BLACK);
        s8.setForeground(Color.WHITE);
        s8.addActionListener(this);

        s9 = new JButton("x^3");
        s9.setText("<html><b>x<sup>3</sup></b><br></html>");
        s9.setBounds(x3, y2, ButtonWidth1, ButtonHeight);
        s9.setBackground(Color.BLACK);
        s9.setForeground(Color.WHITE);
        s9.addActionListener(this);

        s10 = new JButton("x^y");
        s10.setText("<html><b>x<sup>y</sup></b><br></html>");
        s10.setBounds(x4, y2, ButtonWidth1, ButtonHeight);
        s10.setBackground(Color.BLACK);
        s10.setForeground(Color.WHITE);
        s10.addActionListener(this);

        s11 = new JButton("e^x");
        s11.setText("<html><b>e<sup>x</sup></b><br></html>");
        s11.setBounds(x5, y2, ButtonWidth1, ButtonHeight);
        s11.setBackground(Color.BLACK);
        s11.setForeground(Color.WHITE);
        s11.addActionListener(this);

        s12 = new JButton("10^x");
        s12.setText("<html><b>10<sup>x</sup></b><br></html>");
        s12.setBounds(x6, y2, ButtonWidth1, ButtonHeight);
        s12.setBackground(Color.BLACK);
        s12.setForeground(Color.WHITE);
        s12.addActionListener(this);

        s13 = new JButton("x√y");
        s13.setBounds(x2, y3, ButtonWidth1, ButtonHeight);
        s13.setBackground(Color.BLACK);
        s13.setForeground(Color.WHITE);
        s13.addActionListener(this);

        s14 = new JButton("1/x");
        s14.setBounds(x1, y3, ButtonWidth1, ButtonHeight);
        s14.setBackground(Color.BLACK);
        s14.setForeground(Color.WHITE);
        s14.addActionListener(this);

        s15 = new JButton("2√x");
        s15.setBounds(x3, y3, ButtonWidth1, ButtonHeight);
        s15.setBackground(Color.BLACK);
        s15.setForeground(Color.WHITE);
        s15.addActionListener(this);

        s16 = new JButton("3√x");
        s16.setBounds(x4, y3, ButtonWidth1, ButtonHeight);
        s16.setBackground(Color.BLACK);
        s16.setForeground(Color.WHITE);
        s16.addActionListener(this);

        s17 = new JButton("ln");
        s17.setBounds(x5, y3, ButtonWidth1, ButtonHeight);
        s17.setBackground(Color.BLACK);
        s17.setForeground(Color.WHITE);
        s17.addActionListener(this);

        s18 = new JButton("log10");
        s18.setText("<html><b>log<sub>10</sub></b><br></html>");
        s18.setBounds(x6, y3, ButtonWidth1, ButtonHeight);
        s18.setBackground(Color.BLACK);
        s18.setForeground(Color.WHITE);
        s18.addActionListener(this);

        s19 = new JButton("x!");
        s19.setBounds(x1, y4, ButtonWidth1, ButtonHeight);
        s19.setBackground(Color.BLACK);
        s19.setForeground(Color.WHITE);
        s19.addActionListener(this);

        s20 = new JButton("sin");
        s20.setBounds(x2, y4, ButtonWidth1, ButtonHeight);
        s20.setBackground(Color.BLACK);
        s20.setForeground(Color.WHITE);
        s20.addActionListener(this);

        s21 = new JButton("cos");
        s21.setBounds(x3, y4, ButtonWidth1, ButtonHeight);
        s21.setBackground(Color.BLACK);
        s21.setForeground(Color.WHITE);
        s21.addActionListener(this);

        s22 = new JButton("tan");
        s22.setBounds(x4, y4, ButtonWidth1, ButtonHeight);
        s22.setBackground(Color.BLACK);
        s22.setForeground(Color.WHITE);
        s22.addActionListener(this);

        s23 = new JButton("Bi");
        s23.setBounds(x5, y4, ButtonWidth1, ButtonHeight);
        s23.setBackground(Color.BLACK);
        s23.setForeground(Color.WHITE);
        s23.addActionListener(this);

        s24 = new JButton("Hex");
        s24.setBounds(x6, y4, ButtonWidth1, ButtonHeight);
        s24.setBackground(Color.BLACK);
        s24.setForeground(Color.WHITE);
        s24.addActionListener(this);

        s25 = new JButton("Rad");
        s25.setBounds(x1, y5, ButtonWidth1, ButtonHeight);
        s25.setBackground(Color.BLACK);
        s25.setForeground(Color.WHITE);
        s25.addActionListener(this);

        s26 = new JButton("sinh");
        s26.setBounds(x2, y5, ButtonWidth1, ButtonHeight);
        s26.setBackground(Color.BLACK);
        s26.setForeground(Color.WHITE);
        s26.addActionListener(this);

        s27 = new JButton("cosh");
        s27.setBounds(x3, y5, ButtonWidth1, ButtonHeight);
        s27.setBackground(Color.BLACK);
        s27.setForeground(Color.WHITE);
        s27.addActionListener(this);

        s28 = new JButton("tanh");
        s28.setBounds(x4, y5, ButtonWidth1, ButtonHeight);
        s28.setBackground(Color.BLACK);
        s28.setForeground(Color.WHITE);
        s28.addActionListener(this);

        s29 = new JButton("pie");
        s29.setBounds(x5, y5, ButtonWidth1, ButtonHeight);
        s29.setBackground(Color.BLACK);
        s29.setForeground(Color.WHITE);
        s29.addActionListener(this);

        s30 = new JButton("Oct");
        s30.setBounds(x6, y5, ButtonWidth1, ButtonHeight);
        s30.setBackground(Color.BLACK);
        s30.setForeground(Color.WHITE);
        s30.addActionListener(this);

        add(b1);
        add(b2);
        add(b3);
        add(b4);
        add(b5);
        add(b6);
        add(b7);
        add(b8);
        add(b9);
        add(b10);
        add(b11);
        add(b12);
        add(b13);
        add(b14);
        add(b15);
        add(b16);
        add(b17);
        add(b18);
        add(b19);
        add(b20);
        add(b21);
        add(b22);
        add(b23);

        add(s1);
        add(s2);
        add(s3);
        add(s4);
        add(s5);
        add(s6);
        add(s7);
        add(s8);
        add(s9);
        add(s10);
        add(s11);
        add(s12);
        add(s13);
        add(s14);
        add(s15);
        add(s16);
        add(s17);
        add(s18);
        add(s19);
        add(s20);
        add(s21);
        add(s22);
        add(s23);
        add(s24);
        add(s25);
        add(s26);
        add(s27);
        add(s28);
        add(s29);
        add(s30);
        add(dis);
        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("1")) {
            ClearFirstZero();
            dis.setText(dis.getText() + "1");
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("2")) {
            ClearFirstZero();
            dis.setText(dis.getText() + "2");
            MaxTextCount();
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("3")) {
            ClearFirstZero();
            dis.setText(dis.getText() + "3");
            MaxTextCount();
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("4")) {
            ClearFirstZero();
            dis.setText(dis.getText() + "4");
            MaxTextCount();
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("5")) {
            ClearFirstZero();
            dis.setText(dis.getText() + "5");
            MaxTextCount();
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("6")) {
            ClearFirstZero();
            dis.setText(dis.getText() + "6");
            MaxTextCount();
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("7")) {
            ClearFirstZero();
            dis.setText(dis.getText() + "7");
            MaxTextCount();
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("8")) {
            ClearFirstZero();
            dis.setText(dis.getText() + "8");
            MaxTextCount();
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("9")) {
            ClearFirstZero();
            dis.setText(dis.getText() + "9");
            MaxTextCount();
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("0")) {
            ClearFirstZero();
            dis.setText(dis.getText() + "0");
            MaxTextCount();
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("00")) {
            ClearDoubleZero();
            MaxTextCount();
            dis.grabFocus();

        }
        if (e.getActionCommand().equals(".")) {
            ClearFirstZero();
            if (!dis.getText().contains(".")) {
                dis.setText(dis.getText() + ".");
            }
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("+")) {
            operator = "+";
            Calculation();
            dis.setText("0");
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("x")) {
            operator = "x";
            Calculation();
            dis.setText("0");
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("/")) {
            operator = "/";
            Calculation();
            dis.setText("0");
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("-")) {
            operator = "-";
            Calculation();
            dis.setText("0");
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("=")) {
            value2 = Double.parseDouble(dis.getText());
            if (operator.equals("+")) {
                sum = value1 + value2;
                dis.setText(String.valueOf(sum));
            }

            if (operator.equals("x")) {
                sum = value1 * value2;
                dis.setText(String.valueOf(sum));
            }
            if (operator.equals("/")) {
                sum = value1 / value2;
                dis.setText(String.valueOf(sum));
            }
            if (operator.equals("-")) {
                sum = value1 - value2;
                dis.setText(String.valueOf(sum));
            }
            if (operator.equals("Mp")) {
                mplus += Double.parseDouble(dis.getText());
                dis.setText(String.valueOf(mplus));
            }
            if (operator.equals("Mm")) {
                mmine -= Double.parseDouble(dis.getText());
                dis.setText(String.valueOf(mmine));
            }
            if (operator.equals("E")) {

                dis.setText(String.valueOf(E));
            }

            value1 = 0;
            value2 = 0;
            operator = null;
            sum = 0;
            mplus = 0;
            mmine = 0;
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("MC")) {
            memorySave = 0;
            dis.setText("0");
            b9.setEnabled(false);
            b8.setEnabled(false);
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("MR")) {
            dis.setText("" + memorySave);
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("MS")) {
            memorySave = Double.valueOf(dis.getText());
            b9.setEnabled(true);
            b8.setEnabled(true);
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("+/-")) {
            if (dis.getText().contains("-") & dis.getText().length() != 1) {
                String s = dis.getText().substring(1, dis.getText().length());
                dis.setText(s);
            } else if (dis.getText().contains("-") & dis.getText().length() == 1) {
                dis.setText("0");
            } else {
                if (dis.getText().indexOf("0") == 0 & !dis.getText().contains(".")) {
                    dis.setText("-" + dis.getText().substring(1, dis.getText().length()));
                } else {
                    dis.setText("-" + dis.getText());
                }
            }
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("C")) {
            dis.setText("0");
            value1 = 0;
            value2 = 0;
            sum = 0;
            ix = 0;
            iy = 0;
            a = 0;
            mplus = 0;
            mmine = 0;
            ss = 0;
            E = 0;
            mmine = 0;
            dis.grabFocus();
        }
        if (e.getActionCommand().equals("Del")) {
            dis.setText(dis.getText().substring(0, dis.getText().length() - 1));
            if (dis.getText().equals("")) {
                dis.setText("0");
            }
            dis.grabFocus();
        }

        if (e.getActionCommand().equals("X")) {
            X();
            dis.setText("0");
            System.out.println(ix);
            dis.grabFocus();
        }
        if (e.getActionCommand().equals("Y")) {

            Y();
            dis.setText("0");
            System.out.println(iy);
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("M+")) {
            Mplus();
            dis.grabFocus();
        }
        if (e.getActionCommand().equals("M-")) {
            Mmine();
            dis.grabFocus();
        }
        if (e.getActionCommand().equals("Shift")) {
            op = true;//sin,cos,tan
            dis.grabFocus();
        }
        if (e.getActionCommand().equals("%")) {
            sum = (iy / ix) * 100;
            dis.setText("" + sum + "%");
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("E")) {

            if (!dis.getText().equals("")) {
                E = Math.E;
                operator = "e";
                dis.setText("" + E);
            }
            dis.grabFocus();
        }
        if (e.getActionCommand().equals("<html><b>x<sup>3</sup></b><br></html>")) {
            if (!dis.getText().equals("")) {
                value1 = Double.parseDouble(dis.getText());
                sum = value1 * value1 * value1;
                dis.setText(String.valueOf(sum));
            }
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("<html><b>x<sup>2</sup></b><br></html>")) {
            if (!dis.getText().equals("")) {
                value1 = Double.parseDouble(dis.getText());
                sum = value1 * value1;
                dis.setText(String.valueOf(sum));
            }
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("<html><b>x<sup>y</sup></b><br></html>")) {

            double powXY = Math.pow(ix, iy);
            dis.setText("0");
            dis.setText("" + powXY);
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("<html><b>e<sup>x</sup></b><br></html>")) {
            if (!dis.getText().equals("")) {
                value1 = Double.parseDouble(dis.getText());
                double suqart = Math.exp(value1);
                dis.setText("0");
                dis.setText("" + suqart);
            }
            dis.grabFocus();
        }
        if (e.getActionCommand().equals("<html><b>10<sup>x</sup></b><br></html>")) {
            if (!dis.getText().equals("")) {
                double powXY = Math.pow(10, Integer.parseInt(dis.getText()));
                dis.setText("0");
                dis.setText("" + powXY);
            }
            dis.grabFocus();
        }
        if (e.getActionCommand().equals("1/x")) {
            if (!dis.getText().equals("")) {
                value1 = Double.parseDouble(dis.getText());
                value2 = 1 / value1;
                dis.setText("" + value2);
            }
            dis.grabFocus();
        }
        if (e.getActionCommand().equals("2√x")) {
            if (!dis.getText().equals("")) {
                value1 = Double.parseDouble(dis.getText());
                double suqart = Math.sqrt(value1);
                dis.setText("0");
                dis.setText("" + suqart);
            }
            dis.grabFocus();
        }
        if (e.getActionCommand().equals("3√x")) {
            if (!dis.getText().equals("")) {
                value1 = Double.parseDouble(dis.getText());
                double suqart = Math.cbrt(value1);
                dis.setText("0");
                dis.setText("" + suqart);
            }
            dis.grabFocus();
        }
        if (e.getActionCommand().equals("x√y")) {
            iy = 1 / iy;
            double powXYqart = Math.pow(ix, iy);
            dis.setText("0");
            dis.setText("" + powXYqart);
            dis.grabFocus();
        }
        if (e.getActionCommand().equals("ln")) {
            if (!dis.getText().equals("")) {
                value1 = Double.parseDouble(dis.getText());
                double lg10 = Math.log(value1);
                dis.setText("0");
                dis.setText("" + lg10);
            }
            dis.grabFocus();
        }
        if (e.getActionCommand().equals("<html><b>log<sub>10</sub></b><br></html>")) {
            if (!dis.getText().equals("")) {
                value1 = Double.parseDouble(dis.getText());
                double lg10 = Math.log10(value1);
                dis.setText("0");
                dis.setText("" + lg10);
            }
            dis.grabFocus();
        }
        if (e.getActionCommand().equals("x!")) {
            if (dis.getText().equals("")) {
                dis.setText("");
            } else {
                a = fact(Double.parseDouble(dis.getText()));
                dis.setText("");
                dis.setText(dis.getText() + a);
            }
            dis.grabFocus();
        }

        if (e.getActionCommand().equals("sin")) {
            if (!dis.getText().equals("")) {
                value1 = Double.parseDouble(dis.getText());
                double valueRadians = Math.toRadians(value1);
                double sinValue = Math.sin(valueRadians);
                dis.setText("0");
                dis.setText("" + sinValue);
            }
            if (op) {
                value1 = Double.parseDouble(dis.getText());
                double valueRadians = Math.toRadians(value1);
                double sinValue = Math.asin(valueRadians);
                dis.setText("0");
                dis.setText("" + sinValue);
                op = false;
            }
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("cos")) {

            if (!dis.getText().equals("")) {
                value1 = Double.parseDouble(dis.getText());
                double valueRadians = Math.toRadians(value1);
                double cosValue = Math.cos(valueRadians);
                dis.setText("0");
                dis.setText("" + cosValue);
            }
            if (op) {
                value1 = Double.parseDouble(dis.getText());
                double valueRadians = Math.toRadians(value1);
                double sinValue = Math.acos(valueRadians);
                dis.setText("0");
                dis.setText("" + sinValue);
                op = false;
            }
            dis.grabFocus();
        }
        if (e.getActionCommand().equals("tan")) {
            if (!dis.getText().equals("")) {
                value1 = Double.parseDouble(dis.getText());
                double valueRadians = Math.toRadians(value1);
                double tanValue = Math.tan(valueRadians);
                dis.setText("0");
                dis.setText("" + tanValue);
            }
            if (op) {
                value1 = Double.parseDouble(dis.getText());
                double valueRadians = Math.toRadians(value1);
                double sinValue = Math.atan(valueRadians);
                dis.setText("0");
                dis.setText("" + sinValue);
                op = false;
            }
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("Bi")) {
            if (!dis.getText().equals("")) {
                v1 = Integer.parseInt(dis.getText());
                String s1 = Integer.toBinaryString(v1);
                dis.setText(s1);
            }
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("Hex")) {
            if (!dis.getText().equals("")) {
                v1 = Integer.parseInt(dis.getText());
                String s1 = Integer.toHexString(v1);
                dis.setText(s1);
            }
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("Rad")) {
            if (!dis.getText().equals("")) {
                value1 = Double.parseDouble(dis.getText());
                double valueRadians = Math.toRadians(value1);
                dis.setText("0");
                dis.setText("" + valueRadians);
            }
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("sinh")) {
            if (!dis.getText().equals("")) {
                value1 = Double.parseDouble(dis.getText());
                double valueRadians = Math.toRadians(value1);
                double sinhValue = Math.sinh(valueRadians);
                dis.setText("0");
                dis.setText("" + sinhValue);
            }
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("cosh")) {
            if (!dis.getText().equals("")) {
                value1 = Double.parseDouble(dis.getText());
                double valueRadians = Math.toRadians(value1);
                double coshValue = Math.cosh(valueRadians);
                dis.setText("0");
                dis.setText("" + coshValue);
            }
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("tanh")) {
            if (!dis.getText().equals("")) {
                value1 = Double.parseDouble(dis.getText());
                double valueRadians = Math.toRadians(value1);
                double tanhValue = Math.tanh(valueRadians);
                dis.setText("0");
                dis.setText("" + tanhValue);
            }
            dis.grabFocus();

        }
        if (e.getActionCommand().equals("pie")) {
            if (!dis.getText().equals("")) {
                pie();
            }
            dis.grabFocus();
        }
        if (e.getActionCommand().equals("Oct")) {
            if (!dis.getText().equals("")) {
                v1 = Integer.parseInt(dis.getText());
                String s1 = Integer.toOctalString(v1);
                dis.setText(s1);
            }
            dis.grabFocus();

        }
    }

    double fact(double x) {
        int er = 0;
        if (x < 0) {
            er = 20;
            return 0;
        }
        double i, s = 1;
        for (i = 2; i <= x; i += 1.0) {
            s *= i;
        }
        return s;
    }

    void Mplus() {
        if (!dis.getText().equals("")) {
            mplus += Double.parseDouble(dis.getText());
            operator = "Mp";
            dis.setText("0");
        }

    }

    void Mmine() {

        if (!dis.getText().equals("")) {

            if (mmine == 0) {
                mmine = Double.parseDouble(dis.getText());
            } else {
                mmine -= (Double.parseDouble(dis.getText()));
            }
            operator = "Mm";
            dis.setText("0");
        }
    }

    void X() {
        ix = Double.parseDouble(dis.getText());
    }

    void Y() {
        iy = Double.parseDouble(dis.getText());
    }

    public void ClearFirstZero() {
        if (dis.getText().equals("0")) {
            dis.setText("");
        }
    }

    public void ClearDoubleZero() {
        if (dis.getText().equals("0")) {
            dis.setText("0");
        } else {
            dis.setText(dis.getText() + "00");
        }
    }

    public void MaxTextCount() {
        final int MAX_TEXT_LEN = 41;

        if (dis.getText().length() == MAX_TEXT_LEN) {
            String s1 = dis.getText().substring(0, dis.getText().length() - 1);
            dis.setText(s1);
        } else if (dis.getText().length() > MAX_TEXT_LEN) {
            int x = (dis.getText().length() - MAX_TEXT_LEN);
            dis.setText(dis.getText().substring(0, dis.getText().length() - x));
        }
    }

    public void Calculation() {
        if (value1 != 0 & value2 == 0) {
            value2 = Double.parseDouble(dis.getText());
        }
        if (value1 != 0 & value2 != 0) {
            if (operator.equals("+")) {
                value1 += value2;
            }
            if (operator.equals("-")) {
                value1 -= value2;
            }
            if (operator.equals("/")) {
                value1 /= value2;
            }
            if (operator.equals("x")) {
                value1 *= value2;
            }

            value2 = Double.parseDouble(dis.getText());
        } else {
            value1 = Double.parseDouble(dis.getText());
        }
    }

    void pie() {
        value1 = Double.parseDouble(dis.getText());
        dis.setText(String.valueOf(sum = value1 * Math.PI));
    }

    public void keyTyped(KeyEvent e) {

    }

    public void keyPressed(KeyEvent e) {
        MaxTextCount();
        numCalculation(dis, e);
        if (dis.getText().equals("0")) {
            dis.setText("");
        }
    }

    void numCalculation(JTextField feild, KeyEvent e) {

        if (e.getKeyCode() == 96) {
            zero(feild);
        } else if (e.getKeyCode() > 96 & e.getKeyCode() <= 105 | e.getKeyCode() == 8) {
            feild.setEditable(true);
        } else if (e.getKeyCode() == 110) {
            if (!feild.getText().contains(".")) {
                feild.setEditable(true);
            } else {
                feild.setEditable(false);
            }
        } else {
            feild.setEditable(false);
        }
    }

    void zero(JTextField field) {
        if (field.getText().equals("")) {
            field.setEditable(false);
        } else {
            field.setEditable(true);
        }
    }

    public void keyReleased(KeyEvent e) {
        if (dis.getText().equals("")) {
            dis.setText("0");
        }
    }

    public void windowOpened(WindowEvent e) {

    }

    public void windowClosing(WindowEvent e) {
        int x = JOptionPane.showConfirmDialog(rootPane, "Are you want to close", "Window closeing", JOptionPane.YES_NO_OPTION);
        if (x == JOptionPane.YES_OPTION) {
            System.exit(0);
        } else {
            new Calculator();
        }
    }

    public void windowClosed(WindowEvent e) {

    }

    public void windowIconified(WindowEvent e) {

    }

    public void windowDeiconified(WindowEvent e) {

    }

    public void windowActivated(WindowEvent e) {

    }

    public void windowDeactivated(WindowEvent e) {

    }

}
