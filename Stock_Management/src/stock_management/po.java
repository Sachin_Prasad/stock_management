/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stock_management;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Kushan
 */
public class po extends javax.swing.JFrame {

    /**
     * Creates new form po
     */
    public po() {
        initComponents();
        setTitle("BLACK EAGLE | Goods Receive Note");
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setTime();
        po_id.grabFocus();
        user.setText(systemUserData.getCurrentUser());
        ut.setText(systemUserData.getUserID());
        date.setText(new SimpleDateFormat("yyyy MMM dd").format(new Date()));
        Image icon = Toolkit.getDefaultToolkit().getImage("src/images/t_logo.png");
        setIconImage(icon);

        genaratePoId();
        setDataToSuplier();
        remove.setEnabled(false);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLayeredPane1 = new javax.swing.JLayeredPane();
        jButton6 = new javax.swing.JButton();
        jLabel55 = new javax.swing.JLabel();
        user = new javax.swing.JLabel();
        jLabel56 = new javax.swing.JLabel();
        ut = new javax.swing.JLabel();
        jLabel57 = new javax.swing.JLabel();
        date = new javax.swing.JLabel();
        jLabel58 = new javax.swing.JLabel();
        time = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        u_m = new javax.swing.JLabel();
        po_date = new javax.swing.JLabel();
        jScrollPane11 = new javax.swing.JScrollPane();
        po = new javax.swing.JTable();
        po_id = new javax.swing.JTextField();
        jLabel52 = new javax.swing.JLabel();
        jLabel60 = new javax.swing.JLabel();
        rawItem = new javax.swing.JComboBox<>();
        supC = new javax.swing.JComboBox<>();
        jLabel54 = new javax.swing.JLabel();
        item_qty = new javax.swing.JTextField();
        jLabel61 = new javax.swing.JLabel();
        remove = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        c = new javax.swing.JLabel();
        remove1 = new javax.swing.JButton();
        addB = new javax.swing.JButton();
        jLayeredPane2 = new javax.swing.JLayeredPane();
        poDate = new com.toedter.calendar.JDateChooser();
        jLabel10 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        poList = new javax.swing.JList<>();
        DeleteB = new javax.swing.JButton();
        po_des = new javax.swing.JTextField();
        jLabel53 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLayeredPane1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButton6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButton6.setText("Home");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jLayeredPane1.add(jButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(1044, 48, 87, 37));

        jLabel55.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel55.setForeground(new java.awt.Color(255, 255, 255));
        jLabel55.setText("User :");
        jLayeredPane1.add(jLabel55, new org.netbeans.lib.awtextra.AbsoluteConstraints(1164, 12, -1, -1));

        user.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        user.setForeground(new java.awt.Color(255, 255, 255));
        user.setText(" ");
        jLayeredPane1.add(user, new org.netbeans.lib.awtextra.AbsoluteConstraints(1204, 11, 148, -1));

        jLabel56.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel56.setForeground(new java.awt.Color(255, 255, 255));
        jLabel56.setText("User Type :");
        jLayeredPane1.add(jLabel56, new org.netbeans.lib.awtextra.AbsoluteConstraints(1164, 39, -1, -1));

        ut.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        ut.setForeground(new java.awt.Color(255, 255, 255));
        ut.setText(" ");
        jLayeredPane1.add(ut, new org.netbeans.lib.awtextra.AbsoluteConstraints(1241, 38, 80, -1));

        jLabel57.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel57.setForeground(new java.awt.Color(255, 255, 255));
        jLabel57.setText("Date :");
        jLayeredPane1.add(jLabel57, new org.netbeans.lib.awtextra.AbsoluteConstraints(1164, 66, -1, -1));

        date.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        date.setForeground(new java.awt.Color(255, 255, 255));
        date.setText(" ");
        jLayeredPane1.add(date, new org.netbeans.lib.awtextra.AbsoluteConstraints(1207, 65, 114, -1));

        jLabel58.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel58.setForeground(new java.awt.Color(255, 255, 255));
        jLabel58.setText("Time :");
        jLayeredPane1.add(jLabel58, new org.netbeans.lib.awtextra.AbsoluteConstraints(1164, 93, 37, -1));

        time.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        time.setForeground(new java.awt.Color(255, 255, 255));
        time.setText(" ");
        jLayeredPane1.add(time, new org.netbeans.lib.awtextra.AbsoluteConstraints(1207, 92, 114, -1));

        jLabel2.setBackground(new java.awt.Color(0, 0, 102));
        jLabel2.setFont(new java.awt.Font("Sylfaen", 1, 50)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel2.setText("  Purchasing Oder");
        jLabel2.setOpaque(true);
        jLayeredPane1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1350, 124));

        u_m.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        u_m.setForeground(new java.awt.Color(0, 51, 102));
        u_m.setText(" ");
        jLayeredPane1.add(u_m, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 250, 158, 40));

        po_date.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        po_date.setForeground(new java.awt.Color(0, 51, 102));
        po_date.setText(" ");
        jLayeredPane1.add(po_date, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 180, 200, 30));

        po.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Item ID", "Name", "Qty"
            }
        ));
        po.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                poMousePressed(evt);
            }
        });
        jScrollPane11.setViewportView(po);

        jLayeredPane1.add(jScrollPane11, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 342, 980, 334));

        po_id.setEditable(false);
        po_id.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLayeredPane1.add(po_id, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 184, 205, 30));

        jLabel52.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel52.setText("PO ID");
        jLayeredPane1.add(jLabel52, new org.netbeans.lib.awtextra.AbsoluteConstraints(22, 163, 55, -1));

        jLabel60.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel60.setText("Item");
        jLayeredPane1.add(jLabel60, new org.netbeans.lib.awtextra.AbsoluteConstraints(22, 232, 55, -1));

        rawItem.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        rawItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rawItemActionPerformed(evt);
            }
        });
        rawItem.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                rawItemKeyPressed(evt);
            }
        });
        jLayeredPane1.add(rawItem, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 255, 360, 30));

        supC.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        supC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                supCActionPerformed(evt);
            }
        });
        supC.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                supCKeyPressed(evt);
            }
        });
        jLayeredPane1.add(supC, new org.netbeans.lib.awtextra.AbsoluteConstraints(311, 183, 373, 30));

        jLabel54.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel54.setText("Supplier");
        jLayeredPane1.add(jLabel54, new org.netbeans.lib.awtextra.AbsoluteConstraints(298, 163, -1, -1));

        item_qty.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        item_qty.setName("Item Qty"); // NOI18N
        item_qty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                item_qtyKeyPressed(evt);
            }
        });
        jLayeredPane1.add(item_qty, new org.netbeans.lib.awtextra.AbsoluteConstraints(425, 256, 110, 30));

        jLabel61.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel61.setText("Qty");
        jLayeredPane1.add(jLabel61, new org.netbeans.lib.awtextra.AbsoluteConstraints(415, 232, 55, -1));

        remove.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        remove.setForeground(new java.awt.Color(153, 0, 0));
        remove.setText("Remove");
        jLayeredPane1.add(remove, new org.netbeans.lib.awtextra.AbsoluteConstraints(374, 311, -1, -1));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setText("Count");
        jLayeredPane1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 320, -1, -1));

        c.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        c.setForeground(new java.awt.Color(0, 0, 102));
        c.setText("0");
        jLayeredPane1.add(c, new org.netbeans.lib.awtextra.AbsoluteConstraints(940, 320, 42, -1));

        remove1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        remove1.setForeground(new java.awt.Color(153, 0, 0));
        remove1.setText("Clear All");
        remove1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                remove1ActionPerformed(evt);
            }
        });
        jLayeredPane1.add(remove1, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 310, 120, -1));

        addB.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        addB.setForeground(new java.awt.Color(0, 51, 0));
        addB.setText("Done");
        addB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addBActionPerformed(evt);
            }
        });
        jLayeredPane1.add(addB, new org.netbeans.lib.awtextra.AbsoluteConstraints(1030, 560, 282, 75));

        jLayeredPane2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Search Data", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Sylfaen", 0, 18))); // NOI18N

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel10.setText("Date");

        jButton3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton3.setText("Show");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        poList.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        poList.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        poList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                poListMousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(poList);

        DeleteB.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        DeleteB.setForeground(new java.awt.Color(153, 0, 0));
        DeleteB.setText("Delete");
        DeleteB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DeleteBActionPerformed(evt);
            }
        });

        jLayeredPane2.setLayer(poDate, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jLabel10, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jButton3, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jScrollPane2, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(DeleteB, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLayeredPane2Layout = new javax.swing.GroupLayout(jLayeredPane2);
        jLayeredPane2.setLayout(jLayeredPane2Layout);
        jLayeredPane2Layout.setHorizontalGroup(
            jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane2Layout.createSequentialGroup()
                .addComponent(jLabel10)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jLayeredPane2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 277, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jLayeredPane2Layout.createSequentialGroup()
                        .addComponent(poDate, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton3)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jLayeredPane2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(DeleteB, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82))
        );
        jLayeredPane2Layout.setVerticalGroup(
            jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(poDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(DeleteB, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jLayeredPane1.add(jLayeredPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(1001, 195, -1, 268));

        po_des.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        po_des.setName("Description"); // NOI18N
        po_des.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                po_desKeyPressed(evt);
            }
        });
        jLayeredPane1.add(po_des, new org.netbeans.lib.awtextra.AbsoluteConstraints(1010, 500, 300, 30));

        jLabel53.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel53.setText("Description");
        jLayeredPane1.add(jLabel53, new org.netbeans.lib.awtextra.AbsoluteConstraints(1000, 480, 80, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLayeredPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLayeredPane1)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        //        Home h = new Home();
        //        h.setVisible(true);
        //        dispose();
    }//GEN-LAST:event_jButton6ActionPerformed

    private void poMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_poMousePressed
        if (evt.getClickCount() == 1) {
            remove.setEnabled(true);
        }
    }//GEN-LAST:event_poMousePressed

    private void rawItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rawItemActionPerformed
        try {
            ResultSet rs = DB.search("select m.u_measurement from item i inner join unit_measurement m on i.unit_measurement=m.um_id where i.itemid='" + rawItem.getSelectedItem().toString().trim().split("-")[0] + "'");
            while (rs.next()) {
                u_m.setText(rs.getString("u_measurement"));
            }
            item_qty.grabFocus();
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "DB error..!!");
        }
    }//GEN-LAST:event_rawItemActionPerformed

    private void rawItemKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rawItemKeyPressed
        if (evt.getKeyCode() == 10) {
            try {
                ResultSet rs = DB.search("select m.u_measurement from item i inner join unit_measurement m on i.unit_measurement=m.um_id where i.itemid='" + rawItem.getSelectedItem().toString().trim().split("-")[0] + "'");
                while (rs.next()) {
                    u_m.setText(rs.getString("u_measurement"));
                }
                item_qty.grabFocus();
            } catch (Exception e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(this, "DB error..!!");
            }
        }
        if (evt.getKeyCode() == (KeyEvent.VK_SHIFT)) {
            po_des.grabFocus();
        }
    }//GEN-LAST:event_rawItemKeyPressed

    private void supCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_supCActionPerformed
        try {
            ResultSet rs2 = DB.search("select itemid,name,buying_price from item where suplier='" + (supC.getSelectedItem().toString().trim().split("-")[0]) + "'");
            Vector v = new Vector();
            while (rs2.next()) {
                v.add(rs2.getString("itemid") + "-" + rs2.getString("name") + "-" + rs2.getString("buying_price") + " Rs/-");
            }
            rawItem.setModel(new DefaultComboBoxModel(v));
            rawItem.grabFocus();
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "DB error..!!");
        }
    }//GEN-LAST:event_supCActionPerformed

    private void supCKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_supCKeyPressed
        if (evt.getKeyCode() == 10) {
            try {
                ResultSet rs2 = DB.search("select itemid,name,buying_price from item where suplier='" + (supC.getSelectedItem().toString().trim().split("-")[0]) + "'");
                Vector v = new Vector();
                while (rs2.next()) {
                    v.add(rs2.getString("itemid") + "-" + rs2.getString("name") + "-" + rs2.getString("buying_price") + " Rs/-");
                }
                rawItem.setModel(new DefaultComboBoxModel(v));
                rawItem.grabFocus();
            } catch (Exception e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(this, "DB error..!!");
            }
        }
    }//GEN-LAST:event_supCKeyPressed

    private void item_qtyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_item_qtyKeyPressed
        if (evt.getKeyCode() == 10) {
            if (item_qty.getText().trim().equals("") | u_m.getText().trim().equals("")) {
                if (u_m.getText().trim().equals("")) {
                    rawItem.grabFocus();
                } else {
                    item_qty.grabFocus();
                }
            } else {
                addDatatoTable();
                rawItem.grabFocus();
            }
        }
        if (evt.getKeyCode() == (KeyEvent.VK_LEFT)) {
            rawItem.grabFocus();
        }
    }//GEN-LAST:event_item_qtyKeyPressed

    private void remove1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_remove1ActionPerformed
            clearAll();
    }//GEN-LAST:event_remove1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        if (poDate.getDate() != null) {
            try {
                ResultSet rs = DB.search("select po_id,date,user from po where date='" + new SimpleDateFormat("yyyy-MM-dd").format(poDate.getDate()) + "' AND stat='1'");
                Vector v = new Vector();
                while (rs.next()) {
                    v.add(rs.getString("po_id") + "-" + rs.getString("date") + "-" + rs.getString("user"));
                    poList.setListData(v);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toolkit.getDefaultToolkit().beep();
                JOptionPane.showMessageDialog(this, "Data Search Error..!!", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            Toolkit.getDefaultToolkit().beep();
            JOptionPane.showMessageDialog(this, "Please Select a Date !");
            poDate.grabFocus();
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void poListMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_poListMousePressed
        if (evt.getClickCount() == 1) {
            DeleteB.setEnabled(true);
            addB.setEnabled(false);
            try {
                ResultSet rs = DB.search("select p.po_id,p.date,p.description,p.suplier,s.shop_name,s.city from po p inner join suplier s on p.suplier=s.supid where p.po_id='" + poList.getSelectedValue().trim().split("-")[0] + "'");
                while (rs.next()) {
                    po_id.setText(rs.getString("po_id"));
                    po_date.setText("Date : " + rs.getString("date"));
                    po_des.setText(rs.getString("description"));
                    supC.setSelectedItem(rs.getString("suplier") + " - " + rs.getString("shop_name") + " - " + rs.getString("city"));
                }

                DefaultTableModel d = (DefaultTableModel) po.getModel();
                for (int i = 0; i < d.getRowCount();) {
                    d.removeRow(i);
                }
                ResultSet rs2 = DB.search("select p.itemid,p.qty,i.name,m.u_measurement from po_item p inner join item i on p.itemid=i.itemid inner join unit_measurement m on i.unit_measurement=m.um_id where p.po_id='" + poList.getSelectedValue().trim().split("-")[0] + "'");
                while (rs2.next()) {
                    Vector v = new Vector();
                    v.add(rs2.getString("itemid"));
                    v.add(rs2.getString("name"));
                    v.add(rs2.getString("qty") + " " + rs2.getString("u_measurement"));
                    d.addRow(v);
                }
                c.setText("" + po.getRowCount());
                DeleteB.setEnabled(true);
            } catch (Exception e) {
                e.printStackTrace();
                Toolkit.getDefaultToolkit().beep();
                JOptionPane.showMessageDialog(this, "Data Search Error..!!", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_poListMousePressed

    private void DeleteBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DeleteBActionPerformed
        int x = JOptionPane.showConfirmDialog(rootPane, "Are you Sure ??", "Delete PO !!", JOptionPane.YES_NO_OPTION);
        if (x == JOptionPane.YES_OPTION) {
            try {
                DB.iud("update po set stat='0' where po_id='" + poList.getSelectedValue().trim().split("-")[0] + "'");
                JOptionPane.showMessageDialog(this, "Done....!");
                clearAll();
            } catch (Exception e) {
                e.printStackTrace();
                Toolkit.getDefaultToolkit();
                JOptionPane.showMessageDialog(this, "Delete Error...!", "error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_DeleteBActionPerformed

    private void addBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addBActionPerformed
        if (checkEmptyField(po_id, po_des)) {
            try {
                DB.iud("insert into po(po_id,user,suplier,date,description) values('" + po_id.getText().trim().toUpperCase() + "','" + systemUserData.getUserID() + "','" + (supC.getSelectedItem().toString().trim().split("-")[0]) + "',"
                        + "'" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + "','" + po_des.getText() + "')");
                for (int row = 0; row < po.getRowCount(); row++) {
                    DB.iud("insert into po_item(itemid,po_id,qty) values('" + po.getValueAt(row, 0).toString() + "','" + po_id.getText().trim().toUpperCase() + "','" + Double.parseDouble(po.getValueAt(row, 2).toString().trim().split(" ")[0]) + "')");
                }
                JOptionPane.showMessageDialog(this, "Done..!!");
                clearAll();
            } catch (Exception e) {
                e.printStackTrace();
                Toolkit.getDefaultToolkit().beep();
                JOptionPane.showMessageDialog(this, "Data add Error..!!");
            }
        }
    }//GEN-LAST:event_addBActionPerformed

    private void po_desKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_po_desKeyPressed
        if (evt.getKeyCode() == 10) {
            if (po_des.getText().trim().equals("")) {
                po_des.grabFocus();
            } else {
                addB.grabFocus();
            }
        }
    }//GEN-LAST:event_po_desKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(po.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(po.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(po.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(po.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new po().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton DeleteB;
    private javax.swing.JButton addB;
    private javax.swing.JLabel c;
    private javax.swing.JLabel date;
    private javax.swing.JTextField item_qty;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JLayeredPane jLayeredPane2;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable po;
    private com.toedter.calendar.JDateChooser poDate;
    private javax.swing.JList<String> poList;
    private javax.swing.JLabel po_date;
    private javax.swing.JTextField po_des;
    private javax.swing.JTextField po_id;
    private javax.swing.JComboBox<String> rawItem;
    private javax.swing.JButton remove;
    private javax.swing.JButton remove1;
    private javax.swing.JComboBox<String> supC;
    private javax.swing.JLabel time;
    private javax.swing.JLabel u_m;
    private javax.swing.JLabel user;
    private javax.swing.JLabel ut;
    // End of variables declaration//GEN-END:variables
private void setTime() {
        Timer t = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Date d = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
                    time.setText(sdf.format(d));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        t.start();
    }

    private void genaratePoId() {
        try {
            ResultSet rs = DB.search("select count('po_id') as count from po");
            if (rs.next()) {
                int count = rs.getInt("count");
                count++;
                String id = "";
                if (count < 10) {
                    id = "PO00" + count;
                } else if (count < 100) {
                    id = "PO0" + count;
                } else {
                    id = "PO" + count;
                }
                po_id.setText(id);
            }
            supC.grabFocus();
        } catch (Exception e) {
            e.printStackTrace();
            Toolkit.getDefaultToolkit().beep();
            JOptionPane.showMessageDialog(this, "DB Error!");
        }
    }

    private void setDataToSuplier() {
        try {
            ResultSet rs = DB.search("select supid,shop_name,city from suplier where stat='1'");
            Vector v = new Vector();
            while (rs.next()) {
                String dep = rs.getString("supid") + " - " + rs.getString("shop_name") + " - " + rs.getString("city");
                v.add(dep);
            }
            supC.setModel(new DefaultComboBoxModel(v));
        } catch (Exception e) {
            e.printStackTrace();
            Toolkit.getDefaultToolkit().beep();
            JOptionPane.showMessageDialog(this, "DB Error!");
        }
    }

    private void addDatatoTable() {
        if (checkEmptyField(po_id, item_qty)) {
            if (!checkDuplicatesOtherPoitem(rawItem.getSelectedItem().toString().trim().split("-")[0])) {
                try {
                    DefaultTableModel d = (DefaultTableModel) po.getModel();
                    Vector v = new Vector();
                    v.add(rawItem.getSelectedItem().toString().trim().split("-")[0]);
                    v.add(rawItem.getSelectedItem().toString().trim().split("-")[1]);
                    v.add(item_qty.getText().trim() + " " + u_m.getText());
                    d.addRow(v);
                    clearFeilds();
                    c.setText("" + po.getRowCount());
                } catch (Exception e) {
                    e.printStackTrace();
                    Toolkit.getDefaultToolkit().beep();
                    JOptionPane.showMessageDialog(this, "Error..!!");
                }
            } else {
                double nqty = Double.parseDouble(item_qty.getText().trim()) + qty;
                try {
                    DefaultTableModel d = (DefaultTableModel) po.getModel();
                    Vector v = new Vector();
                    v.add(rawItem.getSelectedItem().toString().trim().split("-")[0]);
                    v.add(rawItem.getSelectedItem().toString().trim().split("-")[1]);
                    v.add(nqty + " " + u_m.getText());
                    d.addRow(v);
                    clearFeilds();
                    c.setText("" + po.getRowCount());
                } catch (Exception e) {
                    e.printStackTrace();
                    Toolkit.getDefaultToolkit().beep();
                    JOptionPane.showMessageDialog(this, "Error..!!");
                }
            }
        }
    }
    double qty;

    private boolean checkDuplicatesOtherPoitem(String itemID) {
        DefaultTableModel dtm = (DefaultTableModel) po.getModel();
        boolean availability = false;
        for (int row = 0; row < po.getRowCount(); row++) {
            String tableItemId = po.getValueAt(row, 0).toString();
            if (itemID.equals(tableItemId)) {
                qty = Double.parseDouble(po.getValueAt(row, 2).toString().split(" ")[0]);
                dtm.removeRow(row);
                availability = true;
                break;
            } else {
                continue;
            }
        }
        if (availability) {
            return true;
        } else {
            return false;
        }
    }

    public boolean checkEmptyField(JTextField... fields) {
        for (JTextField field : fields) {
            if (field.getText().trim().isEmpty()) {
                JOptionPane.showMessageDialog(this, field.getName() + " field is empty!!!...", "Empty Field!.", JOptionPane.ERROR_MESSAGE);
                field.grabFocus();
                return false;
            }
        }
        return true;
    }

    private void clearFeilds() {
        item_qty.setText("");
        u_m.setText(" ");
        remove.setEnabled(false);
        rawItem.grabFocus();
    }

    private void clearAll() {
        item_qty.setText("");
        u_m.setText(" ");
        remove.setEnabled(false);
        DeleteB.setEnabled(false);
        addB.setEnabled(true);
        genaratePoId();
        setDataToSuplier();
        rawItem.setModel(new DefaultComboBoxModel());
        DefaultTableModel d = (DefaultTableModel) po.getModel();
        d.setRowCount(0);
        poDate.setDate(null);
        poList.setListData(new String[0]);
        po_date.setText(" ");
        c.setText("0");
        addB.setEnabled(true);
        po_des.setText("");
    }
}
