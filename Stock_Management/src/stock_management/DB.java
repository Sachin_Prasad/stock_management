/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stock_management;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Kushan
 */
public class DB {
    private static Connection con;
    
    private static void init()throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        con = DriverManager.getConnection("jdbc:mysql://localhost:3308/stock_management", "root", "123");
    }
    
    public static Connection getConnection()throws SQLException, ClassNotFoundException{
        if(con == null){
            init();
        }
        return con;
    }
    
    public static void iud(String sql)throws Exception{
        if(con == null){
            init();
        }
        con.createStatement().executeUpdate(sql);
    }
    
    public static ResultSet search(String sql)throws Exception{
        if(con == null){
            init();
        }
        return con.createStatement().executeQuery(sql);
    }
    
    public static PreparedStatement getnewPreparedStatement(String sql) throws ClassNotFoundException, SQLException {
        if (con == null) {
            init();
        }
        PreparedStatement ps = con.prepareStatement(sql);
        return ps;
    }
}
